This is a repo to get started with GitLab,  
  
  
git-cheat-sheet.pdf contains commands that become handy while working with the git commandline, this is available at: https://about.gitlab.com/images/press/git-cheat-sheet.pdf  
  
gitLABrepocreate.sh contains a bash script that creates a new repository using the API v4.  
to get it working you need to modify the line 7 ( token="ENTER YOUR GITLAB ACCESS TOKEN HERE!!!" ) and enter your access token.  
to update it to a new API version, if not done by myself (sorry) e.g. when you get a http 410 error, modify the line 47 to a higher /api/v*/projects. This is the curl extra header included in the request.  
  
Also have a look at at the "Start using Git on the command line" tutorial on https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

  
USAGE:  
git-cheat-sheet.pdf is just a .pdf file, open it however you like to  

gitLABrepocreate.sh:
  
bash gitLABrepocreate.sh -n REPO-NAME    
you need to call it with the bash command not sh!  
  
  
  
CREDITS:
GitLab itself for the git-cheat-sheet.pdf available at: https://about.gitlab.com/images/press/git-cheat-sheet.pdf  
Some commentor on Codewall named saccy that provided the code for gitLABrepocreate.sh using API/v3 which I upgraded to v4. (described at line 8 in this README.md)  

BY ALL MEANS:  
STEAL THIS REPO as long as you follow the licencing and keep the credits as they are. Or don't I am not the cyber police...
