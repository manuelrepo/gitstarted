This was done as a tut for reddit.com/u/Squiff and their awesome blender animation, change names according to your desires!
  
  
  
Create an Account on GitLab or Github 
   

https://gitlab.com/users/sign_up (can be selfhosted, even if not prefered by the libre opensource community) 
https://github.com/signup (owned by Microsoft)   
  
  
Add a Repoistory   
   
In this example called "Brain Pain", change the LICENSE.md and README.md according to the Repo Name  
   
   
GitLab:    
https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project  
  
GitHub:    
https://docs.github.com/en/get-started/quickstart/create-a-repo
  
  
  
Create a License file:  


GitLab:   
Either press Add LICENSE or look into the tutorial to use the webeditor to make a new LICENSE.md
https://docs.gitlab.com/ee/user/project/repository/web_editor.html  
  
GitHub: https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/adding-a-license-to-a-repository   
   
Example for GPL: 
Name of Creator in this example: Squiff    
Name of Repo in this example: Brain Pain 
(Change according to your name/repo name)  
  
Copy it from next line---
Squiff, hereby disclaims all copyright interest in the program “Brain Pain” (which deconstructs trees) written by Squiff.

signature of Squiff   
   
   
This file is part of Brain Pain.

Brain Pain is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Brain Pain is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Brain Pain. If not, see <https://www.gnu.org/licenses/>.    
   
   
---copy until the prior line
   
   
   
Create a README.md:   
  
This will have your description shown when the repo link is called


GitLab: 
Either press Add README or look into the tutorial to use the webeditor to make a new README.md
https://docs.gitlab.com/ee/user/project/repository/web_editor.html    
   
GitHub:  
Press Add File and call it README.md 
https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/customizing-your-repository/about-readmes  
   


Now add all the files you want to add:  

GitLab:   
https://docs.gitlab.com/ee/user/project/repository/web_editor.html   
  
GitHub:  
https://docs.github.com/en/repositories/working-with-files/managing-files/adding-a-file-to-a-repository   
  
  

You did it! You now have your own Repository!
Be sure the Repo is public otherwise only you can see it.
GitLab: https://docs.gitlab.com/ee/public_access/public_access.html
GitHub: https://docs.github.com/en/repositories/managing-your-repositorys-settings-and-features/managing-repository-settings/setting-repository-visibility


  
This will be beautified soonish™   
I don't care about my own credits but your GPL compliant licensing though...   
Anyway as already said in the README.md of gitstarted: BY ALL MEANS STEAL THIS REPO!
